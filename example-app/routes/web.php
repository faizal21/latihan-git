<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/table', function () {
    return view('table');
})->name('table');

Route::get('/data-table', function () {
    return view('datatable');
})->name('datatable');

// Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'index'])->name('register');
Route::post('/welcome', [AuthController::class, 'welcome'])->name('welcome');

Route::redirect('/', '/cast');
Route::resource('cast', CastController::class);
