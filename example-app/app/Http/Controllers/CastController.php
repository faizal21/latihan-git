<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{

    public function index()
    {
        $data = DB::table('cast')->get();
        return view('Cast.index', ['data' => $data]);
    }

    public function create()
    {
        return view('Cast.add');
    }

    public function store(Request $request)
    {

        $request->validate([
            'nama' => ['required', 'max:45'],
            'umur' => ['required', 'max:3']
        ]);
        $query = DB::table('cast')->insert([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
            'created_at' => now()
        ]);
        if ($query) {
            return redirect('cast')->with('message', 'Data berhasil ditambahkan ...');
        } else {
            return back()->withInput();
        }
    }

    public function show($id)
    {
        $data = DB::table('cast')->find($id);
        return view('cast.show', [
            'nama' => $data->nama,
            'umur' => $data->umur,
            'bio' => $data->bio,
        ]);
    }
    public function edit($id)
    {
        $data = DB::table('cast')->find($id);
        return view('cast.edit', [
            'id' => $data->id,
            'nama' => $data->nama,
            'umur' => $data->umur,
            'bio' => $data->bio,
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => ['required', 'max:45'],
            'umur' => ['required', 'max:3']
        ]);
        $query = DB::table('cast')->where('id', $id)->update([
            'nama' => $request->nama,
            'bio' => $request->bio,
            'umur' => $request->umur,
            'updated_at' => now()
        ]);
        if ($query) {
            return redirect('cast')->with('message', 'Data berhasil diedit ...');
        } else {
            return back()->withInput();
        }
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('cast')->with('message', 'Data berhasil dihapus ...');
    }
}
