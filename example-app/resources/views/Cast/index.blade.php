@extends('Layouts.template')

@push('style')
    <link rel="stylesheet" href={{ asset('/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}>
    <link rel="stylesheet" href={{ asset('/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}>
    <link rel="stylesheet" href={{ asset('/assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}>
@endpush
@push('script')
    <script src={{ asset('/assets/plugins/datatables/jquery.dataTables.min.js') }}></script>
    <script src={{ asset('/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}></script>
    <script src={{ asset('/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}></script>
    <script src={{ asset('/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}></script>
    <script>
        $(function() {
            $("#list").DataTable();
        });
    </script>
@endpush
@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Cast</h1>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Cast</h3>
                    </div>
                    <div class="card-body">
                        @if (session()->get('message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        <div>
                            <a href={{ url('cast/create') }} class="btn btn-outline-primary">
                                Tambah Cast
                            </a>
                        </div>
                        <hr>
                        <table id="list" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="25%">Nama</th>
                                    <th width="10%">Umur</th>
                                    <th width="40%">Bio</th>
                                    <th width="20%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 0;
                                @endphp
                                @forelse ($data as $row)
                                    @php
                                        $no = $no + 1;
                                    @endphp
                                    <tr>
                                        <td class="text-center">{{ $no }}</td>
                                        <td>{{ $row->nama }}</td>
                                        <td class="text-center">{{ $row->umur }}</td>
                                        <td>{{ $row->bio }}</td>
                                        <td class="text-center">
                                            <div class="d-flex">
                                                <a class="btn btn-sm btn-outline-dark"
                                                    href="/cast/{{ $row->id }}">Lihat</a>
                                                <a class="ml-1 btn btn-sm btn-outline-primary"
                                                    href="/cast/{{ $row->id }}/edit">Edit</a>
                                                <form class="ml-1" action="/cast/{{ $row->id }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <input class="form-control" type="hidden" name="id"
                                                        value="{{ $row->id }}">
                                                    <button class="btn btn-sm btn-outline-danger"
                                                        type="submit">Hapus</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5" class="text-center">Data kosong</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
