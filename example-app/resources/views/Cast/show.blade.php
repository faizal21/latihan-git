@extends('Layouts.template')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Show Cast</h1>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="callout callout-danger">
                    <h5>Nama : <b>{{ $nama }}</b></h5>
                    <hr>
                    <h5>Umur : <b>{{ $umur }}</b></h5>
                    <hr>
                    <h5>Biodata : <b>{{ $bio }}</b></h5>
                </div>
                <a href={{ url('cast') }} class="btn btn-secondary">Kembali</a>
            </div>
        </section>
    </div>
@endsection
