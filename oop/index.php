<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "Name : {$sheep->name}<br>legs : {$sheep->legs}<br>cold blooded : {$sheep->cold_blooded}<br>&nbsp;<br>";

$sungokong = new frog("buduk");
echo "Name : {$sungokong->name}<br>legs : {$sungokong->legs}<br>cold blooded : {$sungokong->cold_blooded}<br>Jump : {$sungokong->jump()}<br>&nbsp;<br>";

$kodok = new Ape("kera sakti");
echo "Name : {$kodok->name}<br>legs : {$kodok->legs}<br>cold blooded : {$kodok->cold_blooded}<br>Yell : {$kodok->yell()}";
